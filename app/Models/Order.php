<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Book;

class Order extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function books()
    {
        return $this->belongsToMany(Book::class)->withPivot('quantity');
    }

    public function getTotalQuantityAttribute()
    {
        $total_quantity  = 0;
        foreach ($this->books as  $book) {
            $total_quantity += $book->pivot->quantity;
        }

        return $total_quantity;
    }
}
