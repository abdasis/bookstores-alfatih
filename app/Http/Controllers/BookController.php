<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Category;
use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('manage-books')) {
                return $next($request);
            }
            abort(403, 'Anda tidak memiliki akses untuk mengakses halaman ini');
        })->except('homePages');
    }
    public function index(Request $request)
    {
        $books = Book::all();
        $categories = Category::all();
        if ($request->title) {
            $books = Book::where('title', 'like', "%$request->title%")->get();
        }
        if ($request->status) {
            $books = Book::where('status', strtoupper($request->status))->get();
        }
        return view('books.index')->withBooks($books)->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Illuminate\Support\Facades\Validator::make($request->all(), [
            "title" => "required|min:5|max:200",
            "description" => "required|min:20|max:1000",
            "author" => "required|min:3|max:100",
            "publisher" => "required|min:3|max:200",
            "price" => "required|digits_between:0,10",
            "stock" => "required|digits_between:0,10",
            "cover" => "required"
        ])->validate();
        $newBook = new Book();
        $newBook->title = $request->title;
        $newBook->slug = Str::slug($request->title, '-');
        $newBook->description = $request->description;
        $newBook->author = $request->author;
        $newBook->price = $request->price;
        $newBook->publisher = $request->publisher;
        $newBook->stock = $request->stock;
        $newBook->status = $request->save_action;
        if ($request->hasFile('cover')) {
            $cover = $request->file('cover');
            $cover_name = $request->title . '-' . $cover->getClientOriginalName();
            $cover->move('cover-books/', $cover_name);
            $newBook->cover = $cover_name;
        }
        $newBook->created_by = Auth::user()->id;
        $newBook->save();

        if ($request->save_action == 'PUBLISH') {
            return redirect()->route('books.create')->with('status', 'Book successfully saved and published');
        } else {
            return redirect()->route('books.create')->with('status', 'Book saved as draft');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $book = Book::find($slug);
        return view('books.show')->withBook($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $categories = Category::all();
        return view('books.edit')->withBook($book)->withCategories($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->slug = Str::slug($request->title, '-');
        $book->description = $request->description;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->price = $request->price;
        $book->stock = $request->stock;
        $book->status = $request->status;
        $book->updated_by = Auth::user()->id;
        if ($request->hasFile('cover')) {
            if ($request->cover && file_exists(public_path('cover-books') . $book->cover)) {
                File::delete(public_path('cover-books/') . $book->cover);
            }

            $cover = $request->file('cover');
            $cover_name = $request->title . '-' . $cover->getClientOriginalName();
            $cover->move('cover-books', $cover_name);
        }

        $book->save();

        $book->categories()->sync($request->categories);
        return redirect()->route('books.index')->with('status', 'Book successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Book::find($id);
        $category->deleted_by = Auth::user()->id;
        $category->status = "DRAFT";
        $category->save();
        $category->delete();
        return redirect()->route('books.index')->with('status', 'Book successfully deleted');
    }

    public function trash(Request $request)
    {
        $books = Book::onlyTrashed()->get();
        if ($request->get('book_title')) {
            $books = Book::where('name', 'like',  "%$request->book_title%")->get();
        }
        return view('books.trash')->withBooks($books);
    }

    public function draft(Request $request)
    {
        $books = Book::where('status', 'DRAFT')->get();
        if ($request->get('book_title')) {
            $books = Book::where('name', 'like',  "%$request->book_title%")->get();
        }
        return view('books.draft')->withBooks($books);
    }

    public function restore($id)
    {
        $book = Book::withTrashed()->find($id);
        if ($book->trashed()) {
            $book->restore();
        } else {
            return redirect()->route('books.index')->with('status', 'Book is not in trash');
        }

        return redirect()->route('books.index')->with('status', 'Book successfully restored');
    }

    public function permanentDelete($id)
    {
        $book = Book::withTrashed()->find($id);
        if ($book->trashed()) {
            $book->forceDelete();
        }
        return redirect()->route('books.index')->with('status', 'Book Permanently Deleted');
    }

    public function homePages()
    {
        $categories =  Category::all();
        $books = Book::all();
        return view('frontend.layouts.home-page')->withCategories($categories)->withBooks($books);
    }

    public function order(Request $request, $books)
    {
        $book = Book::where('slug', $books)->first();
        if ($book->stock < $request->quantity) {
            return redirect()->back()->with('status', 'Jumlah pembelian lebih banyak dari pada stock');
        } else {
            $newOrder = new Order;
            $newOrder->user_id = Auth::user()->id;
            $newOrder->total_price = ($request->quantity * $book->price);
            $newOrder->invoice_number = rand(1, null) . date('dhs');
            $newOrder->status = 'SUBMIT';
            $newOrder->save();
            $bookOrder = Order::find($newOrder->id);
            $book->orders()->attach($bookOrder, ['quantity' => $request->quantity]);
            if ($newOrder) {
                $book->update([
                    'stock' => $book->stock - $request->quantity
                ]);
            }
            return redirect()->back()->with('status', 'Pembelian berhasil di proses');
        }
    }
}
