<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('manage-users')) {
                return $next($request);
            }
            abort(403, 'Anda tidak memiliki akses untuk mengakses halaman ini');
        })->except('registerUser');
    }


    public function index()
    {
        $users = User::all();


        return view('users.index')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->username = $request->username;
        $newUser->roles = json_encode($request->get('roles'));
        $newUser->address = $request->address;
        $newUser->phone = $request->phone;
        $newUser->email = $request->email;
        $newUser->password = Hash::make($request->password);
        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $fileName = $request->get('name') . $file->getClientOriginalName();
            $file->move('avatars', $fileName);
            $newUser->avatar = $fileName;
        }

        $newUser->save();
        return redirect()->route('users.create')->with('status', 'User successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.sunting')->with(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        \Illuminate\Support\Facades\Validator::make($request->all(), [
            "name" => "required|min:5|max:100",
            "roles" => "required",
            "phone" => "required|digits_between:10,12",
            "address" => "required|min:20|max:200",
        ])->validate();
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->roles = json_encode($request->roles);
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->status = $request->status;
        if ($request->file('avatar')) {
            if ($user->avatar && file_exists(storage_path('app/public/avatars' . $user->avatar))) {
                Storage::delete('avatars/' . $user->avatar);
            }

            $file = $request->file('avatar');
            $fileName = date('dmysh') . '_' . $file->getClientOriginalName();
            $file->move('avatars/', $fileName);
            $user->avatar = $fileName;
        }

        $user->save();
        return redirect()->route('users.edit', [$id])->with('status', 'User successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('status', 'User Successfully Deleted');
    }

    public function registerUser(Request $request)
    {
        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->username = $request->username;
        $newUser->roles = json_encode('CUSTOMER');
        $newUser->address = $request->address;
        $newUser->phone = $request->phone;
        $newUser->email = $request->email;
        $newUser->password = Hash::make($request->password);
        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $fileName = $request->get('name') . $file->getClientOriginalName();
            $file->move('avatars', $fileName);
            $newUser->avatar = $fileName;
        }

        $newUser->save();
        return redirect('/');
    }
}
