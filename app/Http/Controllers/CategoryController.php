<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('manage-categories')) {
                return $next($request);
            }
            abort(403, 'Anda tidak memiliki akses untuk mengakses halaman ini');
        });
    }
    public function index(Request $request)
    {
        $categories = Category::all();
        if ($request->get('category_name')) {
            $categories = Category::where('name', 'like',  "%$request->category_name%")->get();
        }

        return view('categories.index')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'category_name' => 'required',
            'category_image' => 'required'
        ])->validate();
        $newCategory = new Category();
        $newCategory->name = $request->get('category_name');
        if ($request->hasFile('category_image')) {
            $category_image = $request->file('category_image');
            $image_name = $request->get('category_name') . $category_image->getClientOriginalName();
            $category_image->move('categories-images', $image_name);
            $newCategory->image = $image_name;
        }
        $newCategory->created_by = Auth::user()->id;
        $newCategory->slug = Str::slug($request->get('category_name'), '-');
        $newCategory->save();
        return redirect()->route('categories.create')->with(
            'status',
            'Category successfully created'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->name = $request->get('category_name');
        $category->slug = Str::slug($request->get('category_name', '-'));
        if ($request->hasFile('category_image')) {
            if ($category->image && file_exists(public_path('categories-images/' . $category->image))) {
                File::delete(public_path('categories-images/' . $category->image));
            }

            $category_image = $request->file('category_image');
            $image_name = $request->get('category_name') . $category_image->getClientOriginalName();
            $category_image->move('categories-images', $image_name);
            $category->image = $image_name;
        }
        $category->updated_by = Auth::user()->id;


        $category->save();
        return redirect()->route('categories.edit', $id)->with('status', 'Category was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->deleted_by = Auth::user()->id;
        $category->delete();
        $category->save();
        return redirect()->route('categories.index')->with('status', 'Category successfully deleted');
    }

    public function trash(Request $request)
    {
        $categories = Category::onlyTrashed()->get();
        if ($request->get('category_name')) {
            $categories = Category::where('name', 'like',  "%$request->category_name%")->get();
        }
        return view('categories.trash')->withCategories($categories);
    }

    public function restore($id)
    {
        $category = Category::withTrashed()->find($id);
        if ($category->trashed()) {
            $category->restore();
        } else {
            return redirect()->route('categories.index')->with('status', 'Category is not in trash');
        }

        return redirect()->route('categories.index')->with('status', 'Category successfully restored');
    }

    public function permanentDelete($id)
    {
        $category = Category::withTrashed()->find($id);
        if ($category->trashed()) {
            $category->forceDelete();
        }
        return redirect()->route('categories.index')->with('status', 'Category Permanently Deleted');
    }
}
