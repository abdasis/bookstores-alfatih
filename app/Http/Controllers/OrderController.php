<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Gate::allows('manage-orders')) {
                return $next($request);
            }
            abort(403, 'Anda tidak memiliki akses untuk mengakses halaman ini');
        });
    }
    public function index(Request $request)
    {
        $status = $request->status;
        $buyer_email = $request->buyer_email;
        $orders = Order::with('user')
            ->with('books')
            ->whereHas('user', function ($query) use ($buyer_email) {
                $query->where('email', 'LIKE', "%$buyer_email%");
            })
            ->where('status', 'LIKE', "%$status%")
            ->get();
        return view('orders.index')->withOrders($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $book = Book::where('slug', $request->books)->first();
        return view('orders.create')->withBook($book);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $order = Order::find($order->id);
        return view('orders.edit')->withOrder($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order = Order::find($order->id);
        $order->status = $request->status;
        $order->save();
        return redirect()->route('orders.edit', [$order->id])->with('status', 'Order status succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Order::find($id);
        $category->delete();
        return redirect()->route('orders.index')->with('status', 'Book successfully deleted');
    }

    public function trash(Request $request)
    {
        $order = Order::onlyTrashed()->get();
        return view('orders.trash')->withOrders($order);
    }


    public function restore($id)
    {
        $order = Order::withTrashed()->find($id);
        if ($order->trashed()) {
            $order->restore();
        } else {
            return redirect()->route('orders.index')->with('status', 'Book is not in trash');
        }

        return redirect()->route('orders.index')->with('status', 'Book successfully restored');
    }

    public function permanentDelete($id)
    {
        $order = Order::withTrashed()->find($id);
        if ($order->trashed()) {
            $order->forceDelete();
        }
        return redirect()->route('order.index')->with('status', 'Book Permanently Deleted');
    }
}
