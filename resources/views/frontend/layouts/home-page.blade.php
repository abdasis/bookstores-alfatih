@extends('frontend.layouts.app')

@section('content')
<div class="container p-2 m-auto">
    <div class="slider-wrapper">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              @foreach ($books as $book)
              <div class="carousel-item {{ $book->id == 1 ? 'active' : '' }}">
                <img class="d-block img-responsive mx-auto" width="100%" height="400px" style="object-fit: cover"  src="{{ asset('cover-books') . '/' . $book->cover }}" alt="First slide">
              </div>
              @endforeach
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
    </div>
</div>

<div class="container mt-5">
    <h3 class="text-center mb-3">List Book</h3>
    <div class="row row-cols-4">
        @foreach ($books as $book)
        <div class="col">
            <div class="card" style="border-top: 4px solid orange">
                <img src="{{ asset('cover-books') . '/' . $book->cover }}" alt="" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ route('orders.create', ['books' => $book->slug]) }}" class="text-orange">{{ $book->title }}</a></h5>
                    <p class="card-text">{{ Str::limit($book->title,40) }}</p>
                    <a href="{{ route('orders.create', ['books' => $book->slug]) }}" class="btn btn-outline-info btn-sm btn-block">Add to cart</a>
                  </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
