@php
    $categories = App\Models\Category::all();
@endphp
<nav class="navbar navbar-expand-lg navbar-dark" style="border-top: 4px solid orange">
    <div class="container p-1">
        <a class="navbar-brand" href="{{ url('/') }}">Alfatih Book Store</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
            @foreach ($categories as $category)
            <li class="nav-item active">
                <a class="nav-link" href="#">{{ $category->name }}</a>
              </li>
            @endforeach
      </ul>
    </div>
    </div>
</nav>
