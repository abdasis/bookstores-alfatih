@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
              <div class="card card-blue">
                <div class="card-header ui-sortable-handle">
                  <h3 class="card-title">Daftar User</h3>
                  <div class="card-tools">
                    <a href="{{ route('users.create') }}">
                        <button class="btn btn-sm btn-tool btn"><i class="fa fa-plus mr-1"></i>Add user</button>
                    </a>
                </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                  <table id="example2" class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Roles</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{!! $user->name !!}
                        </td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td><img src="{{ url('/') . '/avatars/' . $user->avatar }}" alt="" width="50px""></td>
                        <td class="text-center">
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')"
                                class="d-inline" action="{{route('users.destroy', [$user->id])}}" method="POST">
                                @csrf
                                <input
                                    type="hidden"
                                    name="_method"
                                    value="DELETE"
                                />
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-alt "></i></button>
                            </form>
                            <a href="{{ route('users.show', [$user->id]) }}" class="btn btn-success btn-sm">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
    </div>
@endsection


@section('js-plugin')
<script src="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection

@section('css-plugin')
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
