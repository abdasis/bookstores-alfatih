@extends('layouts.app')

@section('title')
    Create Title
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-blue">
                    <div class="card-header ">
                        Tambah User
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form action="{{ route('users.store') }}" method="post" class="bg-white shadow-sm p-4" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group">
                                      <label for="name">Full Name</label>
                                      <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control {{ $errors->first('name') ? 'is-invalid' : '' }}" placeholder="Full Name" aria-describedby="helpId">
                                      @if ($errors->first('name'))
                                        <div class="invalid-feedback">
                                         {{ $errors->first('name') }}
                                        </div>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                      <label for="username">Username</label>
                                      <input value="{{ old('username') }}" type="text" name="username" id="username" class="form-control {{ $errors->first('username') ? 'is-invalid' : '' }}" placeholder="Username" aria-describedby="helpId">
                                      <small id="helpId" class="text-muted">Tidak boleh menggunakan spasi</small>
                                      @if ($errors->first('name'))
                                        <div class="invalid-feedback">
                                         {{ $errors->first('name') }}
                                        </div>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                      <label for="address">Address</label>
                                      <textarea name="address" id="address" cols="30" rows="4" class="form-control {{ $errors->first('address') ? 'is-invalid' : '' }}">{{ old('address') }}</textarea>
                                      @if ($errors->first('address'))
                                        <div class="invalid-feedback">
                                         {{ $errors->first('address') }}
                                        </div>
                                      @endif
                                    </div>


                                    <div class="form-group">
                                        <label for="">Roles</label>
                                    <div class="form-row p-1">
                                        <div class="custom-control custom-checkbox mr-3 ">
                                            <input {{ old('roles') == 'ADMIN' ? 'checked' : '' }} class="custom-control-input {{ $errors->first('roles') ? 'is-invalid' : '' }}" type="checkbox" value="ADMIN" id="ADMIN" name="roles[]" >
                                            <label for="ADMIN" class="custom-control-label">Admin</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mr-3">
                                            <input {{ old('roles') == 'CUSTOMER' ? 'checked' : '' }} class="custom-control-input {{ $errors->first('roles') ? 'is-invalid' : '' }}" type="checkbox" value="CUSTOMER" id="CUSTOMER" name="roles[]" >
                                            <label for="CUSTOMER" class="custom-control-label">Customer</label>
                                        </div>

                                        <div class="custom-control custom-checkbox mr-3">
                                            <input {{ old('roles') == 'STAFF' ? 'checked' : '' }} class="custom-control-input {{ $errors->first('roles') ? 'is-invalid' : '' }}" type="checkbox" value="STAFF" id="STAFF" name="roles[]" >
                                            <label for="STAFF" class="custom-control-label">Staff</label>
                                        </div>
                                    </div>
                                    @if ($errors->first('roles'))
                                        <div class="invalid-feedback">
                                         {{ $errors->first('roles') }}
                                        </div>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                      <label for="phone">Phone Number</label>
                                      <input type="text" value="{{ old('phone') }}" name="phone" id="phone" class="form-control {{ $errors->first('phone') ? 'is-invalid' : '' }} " placeholder="Input phone number">
                                      @if ($errors->first('phone'))
                                        <div class="invalid-feedback">
                                         {{ $errors->first('phone') }}
                                        </div>
                                      @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="avatar">Avatar Image</label>
                                        <div class="custom-file">
                                            <input type="file" name="avatar" class="custom-file-input {{ $errors->first('avatar') ? 'is-invalid' : '' }}" id="customFile">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                          </div>
                                          @if ($errors->first('avatar'))
                                          <div class="invalid-feedback">
                                            {{ $errors->first('name') }}
                                        </div>
                                          @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" value="{{ old('email') }}" name="email" id="email" class="form-control" {{ $errors->first('email') ? 'is-invalid' : '' }} placeholder="Input Active Email">
                                        @if ($errors->first('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" placeholder="Input Password" class="form-control {{ $errors->first('password') ? 'is-invalid' : '' }} ">
                                        @if ($errors->first('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="password_confirmation">Password Confirmation</label>
                                        <input type="password" name="password_confirmation" class="form-control {{ $errors->first('password_confirmation') ? 'is-invalid' : '' }}" placeholder="Password Confirmation" id="password_confirmation">
                                        @if ($errors->first('password_confirmation'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password_confirmation') }}
                                        </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-primary"><i class="fa fa-save mr-1"></i> Save</button>
                                        <button class="btn btn-danger"><i class="fa fa-undo-alt mr-1"></i>Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
