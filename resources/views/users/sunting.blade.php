@extends('layouts.app')

@section('title')
    Create Title
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-blue">
                    <div class="card-header ">
                        Edit User
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form action="{{ route('users.update', $user->id) }}" method="POST" class="bg-white shadow-sm p-4" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <input type="radio" class="custom-radio" name="status" id="active" value="ACTIVE" {{ $user->status == 'ACTIVE' ? 'checked' : '' }}>
                                        <label for="active">Active</label>

                                        <input type="radio" name="status" class="custom-radio" id="inactive" value="INACTIVE" {{ $user->status == 'INACTIVE' ? 'checked' : '' }}>
                                        <label for="inactive">Inactive</label>
                                    </div>
                                    <div class="form-group">
                                      <label for="name">Full Name</label>
                                      <input value="{{ $user->name }}" type="text" name="name" id="name" class="form-control" placeholder="Full Name" aria-describedby="helpId">
                                    </div>

                                    <div class="form-group">
                                      <label for="username">Username</label>
                                      <input type="text" value="{{ $user->username }}" name="username" id="username" class="form-control" placeholder="Username" aria-describedby="helpId">
                                      <small id="helpId" class="text-muted">Tidak boleh menggunakan spasi</small>
                                    </div>

                                    <div class="form-group">
                                      <label for="address">Address</label>
                                      <textarea name="address" id="address" cols="30" rows="4" class="form-control">{{ $user->address }}</textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Roles</label>
                                    <div class="form-row p-1">
                                        <div class="custom-control custom-checkbox mr-3">
                                            <input {{ in_array("ADMIN", json_decode($user->roles)) ? 'checked' : '' }} class="custom-control-input" type="checkbox" value="ADMIN" id="ADMIN" name="roles[]" >
                                            <label for="ADMIN" class="custom-control-label">Admin</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mr-3">
                                            <input {{in_array('CUSTOMER', json_decode($user->roles)) ? 'checked' : ''}} class="custom-control-input" type="checkbox" value="CUSTOMER" id="CUSTOMER" name="roles[]" >
                                            <label for="CUSTOMER" class="custom-control-label">Customer</label>
                                        </div>

                                        <div class="custom-control custom-checkbox mr-3">
                                            <input class="custom-control-input" {{in_array('STAFF', json_decode($user->roles)) ? 'checked' : ''}} type="checkbox" value="STAFF" id="STAFF" name="roles[]" >
                                            <label for="STAFF" class="custom-control-label">Staff</label>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="form-group">
                                      <label for="phone">Phone Number</label>
                                      <input value="{{ $user->phone }} " type="text" name="phone" id="phone" class="form-control" placeholder="Input phone number">
                                    </div>

                                    @if ($user->avatar)
                                        <img src="{{ asset('avatars/') . '/'. $user->avatar }}" alt="" width="120px">
                                    @else
                                        No Avatar
                                    @endif
                                    <div class="form-group">
                                        <label for="avatar">Avatar Image</label>
                                        <div class="custom-file">
                                            <input  type="file" name="avatar" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                          </div>
                                          @if ($errors->first('avatar'))
                                          <div class="invalid-feedback">
                                            {{ $errors->first('name') }}
                                        </div>
                                          @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input value="{{ $user->email }}" type="email" name="email" id="email" class="form-control" placeholder="Input Active Email">
                                    </div>


                                    <div class="form-group">
                                        <button class="btn btn-primary"><i class="fa fa-save mr-1"></i> Update</button>
                                        <button class="btn btn-danger"><i class="fa fa-undo-alt mr-1"></i>Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
