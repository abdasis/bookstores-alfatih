@extends('layouts.app')

@section('title')
    {{ $user->name }}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card-blue bg-white shadow-sm">
                    <div class="card-header">
                        <div class="card-title">
                            Tentang
                        </div>

                    </div>
                    <div class="card-body">
                        <img src="{{ asset('/avatars') . '/' . $user->avatar }}" alt="" class="img-thumbnail rounded mx-auto d-block">
                        <h5 class="text-center mt-2">{{ $user->name }}</h5>
                        <p class="text-center">{{ $user->email }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card-blue bg-white shadow-sm">
                    <div class="card-header">
                        Biodata
                    </div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <tbody>
                                <tr>
                                    <td scope="row">Nama</td>
                                    <td>:</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td scope="row">Email</td>
                                    <td>:</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <td scope="row">Username</td>
                                    <td>:</td>
                                    <td>{{ $user->username }}</td>
                                </tr>
                                <tr>
                                    <td scope="row">Roles</td>
                                    <td>:</td>
                                    <td>
                                        @foreach (json_decode($user->roles) as $role)
                                        @if ($role == 'ADMIN')
                                            <div class="badge badge-danger">{{ $role }}</div>
                                        @elseif ($role == 'CUSTOMER')
                                            <div class="badge badge-info">{{ $role }}</div>
                                        @else
                                            <div class="badge badge-warning">{{ $role }}</div>
                                        @endif
                                        @endforeach
                                     </td>
                                </tr>
                                <tr>
                                    <td scope="row">Address</td>
                                    <td>:</td>
                                    <td>{{ $user->address }}</td>
                                </tr>
                                <tr>
                                    <td scope="row">Phone</td>
                                    <td>:</td>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
