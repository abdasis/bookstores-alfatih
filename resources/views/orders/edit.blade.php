@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Create Book
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 shadow-sm p-5 m-2">
                    @if (session('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <form action="{{ route('orders.update', $order->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="invoice_number">Invoice Number</label>
                            <input disabled type="text" class="form-control" name="invoice_number" value="{{ $order->invoice_number }}">
                        </div>

                        <div class="form-group">
                            <label for="Buyer">Buyer</label>
                            <input disabled type="text" class="form-control" name="buyer" value="{{ $order->user->name }}">
                        </div>

                        <div class="form-group">
                            <label for="created_at">Order Date</label>
                            <input type="text" class="form-control" name="created_at" disabled value="{{ $order->created_at }}">
                        </div>

                        <div class="form-group">
                            <label for="created_at">Books ({{ $order->totalQuantity }})</label>
                            @foreach ($order->books as $book)
                                <div class="inline-block">
                                    <button type="button" class="btn btn-light btn-sm">
                                        {{ $book->title }} <span class="badge badge-light">{{ $book->pivot->quantity }}</span>
                                      </button>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <label for="">Total Price</label>
                            <input disabled type="text" class="form-control" value="{{ $order->total_price }}">
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label><br>
                            <select class="form-control" name="status" id="status">
                                <option {{$order->status == "SUBMIT" ? "selected" : ""}}
                                value="SUBMIT">SUBMIT</option>
                                <option {{$order->status == "PROCESS" ? "selected" : ""}}
                                value="PROCESS">PROCESS</option>
                                <option {{$order->status == "FINISH" ? "selected" : ""}}
                                value="FINISH">FINISH</option>
                                <option {{$order->status == "CANCEL" ? "selected" : ""}}
                                value="CANCEL">CANCEL</option>
                            </select>
                        </div>




                        <button class="btn-primary btn"><i class="fa fa-save mr-1"></i>Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
