@extends('frontend.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                @if (Session::get('status'))
                    <div class="alert alert-warning">
                        {{ Session::get('status') }}
                    </div>
                @endif
                <form action="{{ route('books.order', $book->slug) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="card p-4">
                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                {{-- {{ dd($book) }} --}}
                                <img src="{{ asset('cover-books') . '/' . $book->cover }}" alt="" class="img-thumbnail">
                            </div>
                            <div class="col-md-8">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td scope="row">{{ ('Title') }}</td>
                                            <td>:</td>
                                            <td>{{ $book->title }}</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">{{ ('Description') }}</td>
                                            <td>:</td>
                                            <td>{{ $book->description }}</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">{{ ('Price') }}</td>
                                            <td>:</td>
                                            <td><b>IDR {{ number_format($book->price , 2 , ',', '.')}}</b></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">{{ ('Stock') }}</td>
                                            <td>:</td>
                                            <td>{{ $book->stock }}</td>
                                        </tr>
                                        <tr>
                                            <div class="form-group-sm">
                                                <td scope="row">{{ ('Quantity') }}</td>
                                                <td>:</td>
                                                <td>
                                                    <input required type="number" name="quantity" id="quantity" class="form-control form-control-sm" placeholder="1" aria-describedby="helpId">
                                                </td>
                                            </div>

                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <button class="btn-info btn btn-block" {{ $book->stock == 0 ? 'disabled' : '' }}>Order</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
