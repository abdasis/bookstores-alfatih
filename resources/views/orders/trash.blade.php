@extends('layouts.app')

@section('title')
    List order
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card list-category">
            <div class="card-header">
                <div class="card-title">
                    List Orders
                </div>
                <div class="card-tools">
                    <div class="row">
                        <form action="{{ route('orders.index') }}" class="form-inline">
                            @csrf
                            <div class="form-group input-group-sm">
                                <input type="email" class="form-control mr-2" name="email" placeholder="Filter by email" >
                            </div>
                            <div class="input-group input-group-sm">
                                <select name="status" class="form-control" id="status">
                                    <option value="">ANY</option>
                                    <option {{Request::get('status') == "SUBMIT" ? "selected" :""}} value="SUBMIT">SUBMIT</option>
                                    <option {{Request::get('status') == "PROCESS" ? "selected" : ""}} value="PROCESS">PROCESS</option>
                                    <option {{Request::get('status') == "FINISH" ? "selected" : ""}} value="FINISH">FINISH</option>
                                    <option {{Request::get('status') == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                                </select>
                                <span class="input-group-append">
                                  <button type="submit" class="btn btn-info btn-flat">Filter</button>
                                </span>
                              </div>
                              <a href="{{ route('orders.trash') }}" class="btn btn-danger btn-sm ml-1 mr-1"><i class="fa fa-trash"></i>Trash</a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">{{ session('status') }}</div>
                @endif
                <table id="example2" class="table table-sm table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Invoice Number</th>
                      <th>Status</th>
                      <th>Buyer</th>
                      <th>Total Quantity</th>
                      <th>Order Date</th>
                      <th>Total Price</th>
                      <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $key => $order)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{!! $order->invoice_number !!}
                        <td>
                            @if ($order->status == 'SUBMIT')
                                <div class="badge badge-warning">{{ $order->status }}</div>
                            @elseif($order->status == "PROSES")
                                 <div class="badge badge-info">{{ $order->status }}</div>
                            @elseif($order->status == "FINISH")
                                <div class="badge badge-success">{{ $order->status }}</div>
                            @else
                                <div class="badge badge-danger">{{ $order->status }}</div>
                            @endif
                        </td>
                        <td>{{ $order->user->name }} <br> <small>{{ $order->user->email  }}</small></td>
                        <td>{{ $order->totalQuantity}}</td>
                        <td>{{ $order->created_at}}</td>
                        <td>{{ $order->total_price}}</td>
                        <td class="text-center">
                            <a href="{{ route('orders.restore', $order->id) }}" class="btn btn-info btn-sm"><i class="fa fa-undo-alt"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')"
                                class="d-inline" action="{{route('orders.permanent', $order->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-alt "></i></button>
                            </form>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection

@section('js-plugin')
<script src="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection

@section('css-plugin')
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
