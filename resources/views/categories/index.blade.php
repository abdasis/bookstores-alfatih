@extends('layouts.app')

@section('title')
    List Category
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card list-category">
            <div class="card-header">
                <div class="card-title">
                    List Category
                </div>
                <div class="card-tools">
                    <div class="row">
                        <form action="{{ route('categories.index') }}" class="form-inline">
                            @csrf
                            <div class="input-group input-group-sm">
                                <input type="text" name="category_name" value="{{ Request::get('category_name') }}" class="form-control">
                                <span class="input-group-append">
                                  <button type="submit" class="btn btn-info btn-flat">Filter</button>
                                </span>
                              </div>
                        </form>
                        <a href="{{ route('categories.index') }}" class="m-1">
                            <button class="btn btn-outline-info btn-sm "><i class="fa fa-paper-plane"></i> Published</button>
                        </a>
                        <a href="{{ route('categories.trash') }}" class="m-1">
                            <button class="btn btn-outline-danger btn-sm "><i class="fa fa-trash-alt"></i>Trash</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">{{ session('status') }}</div>
                @endif
                <table id="example2" class="table table-sm table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Slug</th>
                      <th>Image</th>
                      <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $key => $category)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{!! $category->name !!}
                        <td><small>{!!  url('/') . '/' .$category->slug !!}</small>
                        </td>
                        <td><img src="{{ url('/') . '/categories-images/' . $category->image }}" alt="" width="50px""></td>
                        <td class="text-center">
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')"
                                class="d-inline" action="{{route('categories.destroy', $category->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-alt "></i></button>
                            </form>
                            <a href="{{ route('categories.show', [$category->id]) }}" class="btn btn-success btn-sm">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection

@section('js-plugin')
<script src="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection

@section('css-plugin')
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
