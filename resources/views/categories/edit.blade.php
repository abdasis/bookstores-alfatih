@extends('layouts.app')

@section('title')
    Create Category
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="row justify-content-center">
                <div class="col-md-8 shadow-sm p-5 m-5">
                    @if (session('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <form action="{{ route('categories.update', $category->id) }}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="category_name">Category Name</label>
                            <input type="text" class="form-control" placeholder="Category Name" name="category_name" value="{{ $category->name }}">
                        </div>

                        <div class="form-group">
                            @if ($category->image)
                            <img src="{{ asset('/') . 'categories-images/' . $category->image }}" alt="" class="img-thumbnail mx-auto d-block">
                            @else
                            Tidak ada gambar
                            @endif
                            <label for="customFile">Category Image</label>
                            <div class="custom-file">
                                <input type="file" name="category_image" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                              </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-block"><i class="fa fa-save mr-1"></i>Save Category</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
