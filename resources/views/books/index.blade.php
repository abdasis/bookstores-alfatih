@extends('layouts.app')

@section('title')
    List Book
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card list-book">
            <div class="card-header">
                <div class="card-title">
                    List Book
                </div>
                <div class="card-tools">
                    <div class="row">
                        <form action="{{ route('books.index') }}" class="form-inline">
                            @csrf
                            <div class="input-group input-group-sm">
                                <input placeholder="Filter by title" value="{{ Request::get('title') }}" type="text" class="form-control" name="title">
                                <span class="input-group-append">
                                  <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-filter"></i></button>
                                </span>
                              </div>
                        </form>
                        <a href="{{ route('books.index', ['status' => 'publish']) }}" class="m-1">
                            <button class="btn btn-outline-info btn-sm "><i class="fa fa-paper-plane mr-1"></i>All</button>
                        </a>
                        <a href="{{ route('books.index', ['status' => 'draft']) }}" class="m-1">
                            <button class="btn btn-outline-warning btn-sm "><i class="fa fa-bookmark mr-1"></i>Draft</button>
                        </a>
                        <a href="{{ route('books.trash') }}" class="m-1">
                            <button class="btn btn-outline-danger btn-sm "><i class="fa fa-bookmark mr-1"></i>Trashed</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">{{ session('status') }}</div>
                @endif
                <table id="example2" class="table table-sm table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                      <th>Cover</th>
                      <th>Title</th>
                      <th>Author</th>
                      <th>Status</th>
                      <th>Categories</th>
                      <th>Stock</th>
                      <th>Price</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($books as $key => $book)
                    <tr>
                        <td><img src="{{ url('/') . '/cover-books/' . $book->cover }}" alt="" width="50px""></td>
                        <td>{!! $book->title !!}
                        <td>{{ $book->author }}</td>
                        <td>
                            @if ($book->status == 'PUBLISH')
                                <div class="badge badge-success">{{ $book->status }}</div>
                            @else
                                <div class="badge badge-warning">{{ $book->status }}</div>
                            @endif
                        </td>
                        <td>
                            @foreach ($book->categories as $category)
                                <div class="badge badge-light mb-1 d-block">
                                    {{ $category->name }}
                                </div>
                            @endforeach
                        </td>
                        <td>{{ $book->stock }}</td>
                        <td>Rp. {{ number_format($book->price, 2 , ',', '.') }}</td>
                        <td class="text-center">
                            <a href="{{ route('books.edit', $book->id) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i></a>
                            <form onsubmit="return confirm('Delete this user permanently?')"
                                class="d-inline" action="{{route('books.destroy', $book->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-alt "></i></button>
                            </form>
                            <a href="{{ route('categories.show', [$book->id]) }}" class="btn btn-success btn-sm">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection

@section('js-plugin')
<script src="{{ url('/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ url('/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection

@section('css-plugin')
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
