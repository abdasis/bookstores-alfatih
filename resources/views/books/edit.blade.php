@extends('layouts.app')

@section('title')
    Create Category
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Create Book
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 shadow-sm p-5 m-2">
                    @if (session('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <form action="{{ route('books.update' , $book->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" placeholder="Book Title" name="title" value="{{ $book->title }}">
                        </div>
                        <div class="form-group">
                            @if ($book->cover)
                                <img src="{{ asset('cover-books/') . '/' . $book->cover }}" alt="" class="img-thumbnail mx-auto d-block">
                            @endif
                            <label for="customFile">Cover</label>
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                              </div>
                        </div>

                        <div class="form-group">
                            <label for="">Deskription</label>
                            <textarea name="description" id="description" cols="30" rows="6" class="form-control">{{ $book->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="number" name="stock" id="stock" class="form-control" placeholder="Stock" value="{{ $book->stock }}">
                        </div>
                        <div class="form-group">
                            <label for="">Author</label>
                            <input type="text" class="form-control" value="{{ $book->author }}" name="author" placeholder="Author name">
                        </div>

                        <div class="form-group">
                            <label for="">Publisher</label>
                            <input type="text" class="form-control" placeholder="Book Publisher" value="{{ $book->publisher }}" name="publisher">
                        </div>

                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="number" name="price" id="price" value="{{ $book->price }}" class="form-control" placeholder="Book price">
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status" id="status">
                                <option>-- Select --</option>
                                <option value="PUBLISH" {{ $book->status == 'PUBLISH' ? 'selected' : '' }}>Publish</option>
                                <option value="DRAFT" {{ $book->status == 'DRAFT' ? 'selected' : '' }}>Draft</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Category</label>
                            <div class="select2-purple">
                              <select class="select2" multiple="multiple" data-placeholder="Select Category" data-dropdown-css-class="select2-purple" name="categories[]" style="width: 100%;">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>

                        <button class="btn-primary btn"><i class="fa fa-save mr-1"></i>Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css-plugin')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('/') }}/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection

@section('js-plugin')
<script src="{{ url('/') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{ url('/') }}/plugins/select2/js/select2.full.min.js"></script>
<script>
     //Initialize Select2 Elements
     $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    })
</script>
@endsection
