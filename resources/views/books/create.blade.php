@extends('layouts.app')

@section('title')
    Create Category
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Create Book
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 shadow-sm p-5 m-2">
                    @if (session('status'))
                        <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <form action="{{ route('books.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="category_name">Title</label>
                            <input type="text" class="form-control {{ $errors->first('title') ? 'is-invalid' : '' }}" placeholder="Book Title" name="title" value="{{ old('title') }}">
                            <div class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="customFile">Cover</label>
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input {{ $errors->first('cover') }}" id="customFile">
                                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                              </div>
                            <div class="invalid-feedback">
                                {{ $errors->first('cover') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Deskription</label>
                            <textarea name="description" id="description" cols="30" rows="6" class="form-control {{ $errors->has('desciption') ? 'is-invalid' : '' }}">{{ old('description') }}</textarea>
                            <div class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="number" name="stock" id="stock" class="form-control {{ $errors->first('stock') }}" placeholder="Stock" value="{{ old('stock') }}">

                            <div class="invalid-feedback">
                                {{ $errors->first('stock') }}
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="">Author</label>
                            <input type="text" class="form-control" name="author" placeholder="Author name">
                            <div class="invalid-feedback">
                                {{ $errors->first('author') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Publisher</label>
                            <input type="text" class="form-control" placeholder="Book Publisher" value="{{ old('publisher') }}" name="publisher">
                            <div class="invalid-feedback">
                                {{ $errors->first('publisher') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="number" name="price" id="price" value="{{ old('price') }}" class="form-control" placeholder="Book price">
                            <div class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </div>
                        </div>


                        <div class="form-group">
                            <button name="save_action" value="PUBLISH" class="btn btn-info"><i class="fa fa-paper-plane mr-1"></i>Publish</button>
                            <button name="save_action" value="DRAFT" class="btn btn-warning"><i class="fa fa-bookmark mr-1"></i>Save as draft</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
