<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new User;

        $administrator->username = "abdasis";
        $administrator->name = "Abd. Asis";
        $administrator->email = "abdasis@gmail.com";
        $administrator->roles = json_encode(['ADMIN']);
        $administrator->password = Hash::make('rahasia');
        $administrator->avatar = "saat-ini-tidak-ada-file.png";
        $administrator->phone = "081944999994";
        $administrator->address = "Rongdurin, Bangkalan, Madura";
        $administrator->save();
        $this->command->info('User Berhasil Ditambahkan');
    }
}
