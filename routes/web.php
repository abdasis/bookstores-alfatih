<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::post('users/register-user', 'UserController@registerUser')->name('users.register');
Route::resource('users', 'UserController');

Route::delete('/categories/{categories}/permanent', 'CategoryController@permanentDelete')->name('categories.permanent');
Route::get('/categories/{categories}/restore', 'CategoryController@restore')->name('categories.restore');
Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::resource('categories', 'CategoryController');

Route::get('/', 'BookController@homePages');
Route::delete('/books/{books}/permanent', 'BookController@permanentDelete')->name('books.permanent');
Route::get('/books/{books}/restore', 'BookController@restore')->name('books.restore');
Route::get('/books/trash', 'BookController@trash')->name('books.trash');
Route::get('/books/draft', 'BookController@draft')->name('books.draft');
Route::put('/books/order/{books}', 'BookController@order')->name('books.order');
Route::resource('books', 'BookController');

Route::delete('/orders/{orders}/permanent', 'OrderController@permanentDelete')->name('orders.permanent');
Route::get('/orders/{orders}/restore', 'OrderController@restore')->name('orders.restore');
Route::get('/orders/trash', 'OrderController@trash')->name('orders.trash');
Route::resource('orders', 'OrderController');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
